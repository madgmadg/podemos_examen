from django.db import models
from cuentas.models import Cuentas

# Create your models here.

class Transacciones(models.Model):
    id = models.IntegerField(primary_key=True)
    cuenta = models.ForeignKey(Cuentas, models.DO_NOTHING)
    fecha = models.DateTimeField(null=True)
    monto = models.DecimalField(max_digits=15, decimal_places=2)

    class Meta:
        managed = False
        db_table = 'transacciones'