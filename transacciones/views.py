from django.shortcuts import render, redirect
from cuentas.models import Cuentas
from formsClass.clientesform import agregar_pago
from django.db.models import Sum
from django.contrib import messages
from transacciones.models import Transacciones
# Create your views here.

def create(request, id):
    model = Cuentas.objects.filter(id=id)
    if model.count() == 0:
        return redirect('error/404')
    model = model.get()
    form = agregar_pago()
    if request.method == 'POST':
        form = agregar_pago(data=request.POST)
        if form.is_valid():
            monto_pagos = model.transacciones_set.aggregate(Sum('monto'))
            monto_n = monto_pagos['monto__sum']
            monto = request.POST['monto']
            if (float(monto_n) + float(monto)) > model.monto:
                messages.error(request, 'El pago excede la deuda actual!!')
                return redirect('/transacciones/create/' + str(model.id))
            if (float(monto_n) + float(monto)) == model.monto:
                model.estatus = Cuentas.ESTATUS_CERRADA
                model.save()
            transaccion = Transacciones(
                fecha=request.POST['fecha'],
                cuenta_id=model.id,
                monto=monto
            )
            transaccion.save()
            messages.success(request, 'El pago se guardo correctamente!!')
            return redirect('/cuentas/view/' + str(model.id))
    return render(request, 'transacciones/create.html',{
        'model': model,
        'form': form
    })