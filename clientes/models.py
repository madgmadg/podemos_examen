from django.db import models

# Create your models here.

class Clientes(models.Model):
    id = models.CharField(primary_key=True, max_length=7, unique=True)
    nombre = models.CharField(null=True, max_length=60)

    class Meta:
        managed = False
        db_table = 'clientes'