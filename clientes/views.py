from django.shortcuts import render, redirect
from clientes.models import Clientes
from django.http import JsonResponse
from formsClass.clientesform import clientes_form, clientes_form_update
from django.contrib import messages

# Create your views here.

def index(request):
    return render(request, 'clientes/index.html')

def listado(request):
    model = list(Clientes.objects.values())
    return JsonResponse({'data': model}, safe=False)

def create(request):
    form = clientes_form()
    if request.method == "POST":
        form = clientes_form(data=request.POST)
        if form.is_valid():
            model = Clientes(
                id=request.POST['id'].upper(),
                nombre=request.POST['nombre'],
            )
            model.save()
            messages.success(request, 'Creado con éxito!!')
            return redirect('/clientes/index')
    return render(request, 'clientes/create.html', {
        'form': form
    })

def view(request, id):
    model = Clientes.objects.filter(id=id)
    if model.count() == 0:
        return redirect('error/404')
    model = model.get()
    grupos = model.miembros_set.all()
    return render(request, 'clientes/view.html', {
        'model': model,
        'grupos': grupos
    })

def update(request, id):
    model = Clientes.objects.filter(id=id)
    if model.count() == 0:
        return redirect('error/404')
    model = model.get()
    form = clientes_form_update(instance=model)
    if request.method == "POST":
        form = clientes_form_update(data=request.POST)
        if form.is_valid():
            model.nombre = request.POST['nombre']
            model.save()
            messages.success(request, 'Actualizado con éxito!!')
            return redirect('/clientes/view/' + str(model.id))
    return render(request, 'clientes/update.html',{
        'form': form,
        'model': model
    })
