"""podemos URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.contrib.auth.decorators import login_required
from users import views
from clientes import views as clientes_views
from cuentas import views as cuentas_views
from grupos import views as grupos_views
from transacciones import views as transacciones_views

urlpatterns = [
    #path('admin/', admin.site.urls),
    path('login', views.login),
    path('', login_required(views.welcome, login_url='/login'), name='/'),
    path('logout/', login_required(views.logout, login_url = '/login'), name='logout'),
    path('usuarios/index', login_required(views.index, login_url='/login'), name='usuarios/index'),
    path('usuarios/listado', login_required(views.listado, login_url='/login'), name='usuarios/listado'),
    path('usuarios/create', login_required(views.create, login_url='/login'), name='usuarios/create'),
    path('usuarios/view/<int:id>', login_required(views.view, login_url='/login'), name='usuarios/view'),
    path('usuarios/update/<int:id>', login_required(views.update, login_url='/login'), name='usuarios/update'),
    path('error/403', login_required(views.error403, login_url='/login'), name='error/403'),
    path('error/404', login_required(views.error404, login_url='/login'), name='error/404'),
    path('clientes/index', login_required(clientes_views.index, login_url='/login'), name='clientes/index'),
    path('clientes/listado', login_required(clientes_views.listado, login_url='/login'), name='clientes/listado'),
    path('clientes/create', login_required(clientes_views.create, login_url='/login'), name='clientes/create'),
    path('clientes/view/<str:id>', login_required(clientes_views.view, login_url='/login'), name='clientes/view'),
    path('clientes/update/<str:id>', login_required(clientes_views.update, login_url='/login'), name='clientes/update'),
    path('cuentas/index', login_required(cuentas_views.index, login_url='/login'), name='cuentas/index'),
    path('cuentas/listado', login_required(cuentas_views.listado, login_url='/login'), name='cuentas/listado'),
    path('cuentas/create', login_required(cuentas_views.create, login_url='/login'), name='cuentas/create'),
    path('cuentas/view/<str:id>', login_required(cuentas_views.view, login_url='/login'), name='cuentas/view'),

    path('grupos/index', login_required(grupos_views.index, login_url='/login'), name='grupos/index'),
    path('grupos/listado', login_required(grupos_views.listado, login_url='/login'), name='grupos/listado'),
    path('grupos/create', login_required(grupos_views.create, login_url='/login'), name='grupos/create'),
    path('grupos/update/<str:id>', login_required(grupos_views.update, login_url='/login'), name='grupos/update'),
    path('grupos/view/<str:id>', login_required(grupos_views.view, login_url='/login'), name='grupos/view'),
    path('grupos/eliminacliente/<str:id>/<str:id_cliente>', login_required(grupos_views.elimina_cliente, login_url='/login'), name='grupos/eliminacliente'),
    path('grupos/agregarcliente/<str:id>', login_required(grupos_views.agregarcliente, login_url='/login'), name='grupos/agregarcliente'),

    path('transacciones/create/<str:id>', login_required(transacciones_views.create, login_url='/login'), name='transacciones/create'),

]
