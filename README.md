#Instalación de proyecto

**Primero:** Clonar el proyecto (git clone git@gitlab.com:madgmadg/podemos_examen.git podemos)

**Segundo:** Creamos el entorno virtual (virtualenv podemos_env -p python3)

**Tercero:** Encendemos el entorno virtual (.\podemos_env\Scripts\activate)

**Cuarto:** Instalamos las dependencias:
* cd podemos
* pip install -r requirements.txt

**Quinto:** Instalamos nuestra base de datos MySQL importa el archivo llamado podemos_eval.sql que encontrarás en este proyecto

**Sexto:** Dentro de nuestro proyecto ir a la carpeta **podemos**, buscar el archivo settings.py y modificar la siguiente parte de código por los accesos de tu base de datos

```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'podemos_eval',
        'USER': 'root',
        'PASSWORD': '',
        'HOST': 'localhost',
        'PORT': '3306',
    }
}
```

**Séptimo:** Ejecutamos dentro de la carpeta de nuestro proyecto (python manage.py runserver)

**Octavo:** Abrimos nuestro navegador y ponemos la url que nos indica en consola al ejecutar el séptimo paso por lo regular es  http://127.0.0.1:8000/

**Noveno:** Los accesos para ingresar al sistema son:
* Usuario: adelatorre
* Contraseña: Adelatorre123!!

**Pruebas:** Deberás cambiar de rama para ejecutar las pruebas unitarias
* git checkout test
* python manage.py test
