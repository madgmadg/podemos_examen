from django.shortcuts import render, redirect
from django.contrib.auth import logout as do_logout
from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login as do_login
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import user_passes_test
from django.http import JsonResponse
from django.contrib.admin.views.decorators import staff_member_required
from formsClass.usuariosform import usuarioForm

def welcome(request):
    # Si estamos identificados devolvemos la portada
    return render(request, "users/welcome.html")
    # En otro caso redireccionamos al login

def register(request):
    return render(request, "users/register.html")

def login(request):
    form = AuthenticationForm()
    if request.user.is_authenticated:
        return redirect('/')
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        """
        if User.objects.filter( username=str(request.POST['username']) ).exists():
            model = User.objects.get( username=str(request.POST['username']) )
            model.set_password(request.POST['password'])
            model.save()
        """
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                do_login(request, user)
                return redirect('/')

    return render(request, "users/login.html", {'form': form})

def logout(request):
    # Finalizamos la sesión
    do_logout(request)
    # Redireccionamos a la portada
    return redirect('/')

@staff_member_required(login_url='error/403')
def index(request):
    return render(request, 'users/index.html')

@staff_member_required(login_url='error/403')
def listado(request):
    model = list(User.objects.values())
    return JsonResponse({'data': model}, safe=False)

@staff_member_required(login_url='error/403')
def view(request, id):
    try:
        model = User.objects.get(id=id)
    except User.DoesNotExist:
        return redirect('error/404')
    return render(request, 'users/view.html', {'model': model})

@staff_member_required(login_url='error/403')
def update(request, id):
    try:
        model = User.objects.get(id=id)
    except User.DoesNotExist:
        return redirect('error/404')
    form = usuarioForm()
    if request.method == 'POST':
        form = usuarioForm(request.POST)
        if form.is_valid():
            model.email = request.POST['email']
            model.username = request.POST['username']
            model.is_staff = request.POST['is_staff']
            model.is_active = request.POST['is_active']
            model.save()
            messages.success(request, 'Actualizado con éxito!!')
            return redirect('/usuarios/view/' + str(model.id))
        else:
            form = usuarioForm(request.POST)
    return render(request, 'users/update.html', {
        'model': model,
        'form': form,
    })

@staff_member_required(login_url='error/403')
def create(request):
    form = usuarioForm()
    if request.method == 'POST':
        form = usuarioForm(request.POST)
        if form.is_valid():
            model = User(
                email=request.POST['email'],
                username=request.POST['username'],
                is_staff=request.POST['is_staff'],
                is_active=request.POST['is_active'],
            )
            model.save()
            messages.success(request, 'Creado con éxito!!')
            return redirect('/usuarios/view/' + str(model.id))
        else:
            form = usuarioForm(request.POST)
    return render(request, 'users/create.html', {
        'form': form
    })

def error403(request):
    return render(request, 'errores/403.html')
    
def error404(request):
    return render(request, 'errores/404.html')