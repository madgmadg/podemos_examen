from django.db import models
from cuentas.models import Cuentas

# Create your models here.

class Calendariopagos(models.Model):

    NUMERO_PAGOS = 4
    
    ESTATUS_PAGADO = "PAGADO"
    ESTATUS_PARCIAL = "PARCIAL"
    ESTATUS_PENDIENTE = "PENDIENTE"
    ESTATUS_ATRASADO = "ATRASADO"

    id = models.IntegerField(primary_key=True, unique=True)
    cuenta = models.ForeignKey(Cuentas, models.DO_NOTHING)
    num_pago = models.IntegerField(null=True)
    monto = models.DecimalField(max_digits=15, decimal_places=2)
    fecha_pago = models.DateField(null=True)
    estatus = models.CharField(null=True, max_length=15)
    
    class Meta:
        managed = False
        db_table = 'calendariopagos'
        