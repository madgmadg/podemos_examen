from django.apps import AppConfig


class CalendariopagosConfig(AppConfig):
    name = 'calendariopagos'
