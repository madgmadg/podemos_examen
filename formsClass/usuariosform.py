from django import forms
from django.contrib.auth.models import User

class usuarioForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(usuarioForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs.update({'class' : 'form-control'})
        self.fields['username'].widget.attrs.update({'class' : 'form-control'})
        self.fields['is_staff'].widget.attrs.update({'class' : 'form-control'})
        self.fields['is_active'].widget.attrs.update({'class' : 'form-control'})

    email = forms.CharField(required=True, error_messages={'required': 'es obligatorio!'})
    username = forms.CharField(required=True, error_messages={'required': 'es obligatorio!'})
    is_staff = forms.CharField(required=True, error_messages={'required': 'es obligatorio!'})
    is_active = forms.CharField(required=True, error_messages={'required': 'es obligatorio!'})
