from django import forms
from clientes.models import Clientes
from grupos.models import Grupos
from cuentas.models import Cuentas
from django.forms import ModelForm

class clientes_form(forms.Form):
    def __init__(self, *args, **kwargs):
        super(clientes_form, self).__init__(*args, **kwargs)
        self.fields['id'].widget.attrs.update({'class' : 'form-control'})
        self.fields['nombre'].widget.attrs.update({'class' : 'form-control'})

    id = forms.CharField(required=True, max_length=7, min_length=7,error_messages={'required': 'es obligatorio!'})
    nombre = forms.CharField(required=True, max_length=60, error_messages={'required': 'es obligatorio!'})

    def clean_id(self):
        id = self.cleaned_data.get("id").upper()
        if Clientes.objects.filter(id=id).exists():
            raise forms.ValidationError("ya existe")
        return id

class clientes_form_update(ModelForm):
    def __init__(self, *args, **kwargs):
        super(clientes_form_update, self).__init__(*args, **kwargs)
        self.fields['nombre'].widget.attrs.update({'class' : 'form-control'})
    nombre = forms.CharField(required=True, max_length=60, error_messages={'required': 'es obligatorio!'})

    class Meta:
        model = Clientes
        fields = ['nombre']

class grupos_form(forms.Form):
    def __init__(self, *args, **kwargs):
        super(grupos_form, self).__init__(*args, **kwargs)
        self.fields['id'].widget.attrs.update({'class' : 'form-control'})
        self.fields['nombre'].widget.attrs.update({'class' : 'form-control'})

    id = forms.CharField(required=True, max_length=5, min_length=5,error_messages={'required': 'es obligatorio!'})
    nombre = forms.CharField(required=True, max_length=20, error_messages={'required': 'es obligatorio!'})

    def clean_id(self):
        id = self.cleaned_data.get("id").upper()
        if Grupos.objects.filter(id=id).exists():
            raise forms.ValidationError("ya existe")
        return id

class grupos_form_update(ModelForm):
    def __init__(self, *args, **kwargs):
        super(grupos_form_update, self).__init__(*args, **kwargs)
        self.fields['nombre'].widget.attrs.update({'class' : 'form-control'})
    nombre = forms.CharField(required=True, max_length=20, error_messages={'required': 'es obligatorio!'})

    class Meta:
        model = Grupos
        fields = ['nombre']

class agregar_cliente(forms.Form):
    def __init__(self, *args, **kwargs):
        super(agregar_cliente, self).__init__(*args, **kwargs)
        self.fields['cliente_id'].widget.attrs.update({'class' : 'form-control'})

    clientes = [(choice.id, choice.nombre) for choice in Clientes.objects.all()]
    clientes = [('', 'Seleccione')] + clientes
    cliente_id = forms.ChoiceField(required=True, choices=clientes)

class agregar_cuentas(forms.Form):
    def __init__(self, *args, **kwargs):
        super(agregar_cuentas, self).__init__(*args, **kwargs)
        self.fields['grupo_id'].widget.attrs.update({'class' : 'form-control'})
        self.fields['estatus'].widget.attrs.update({'class' : 'form-control'})
        self.fields['monto'].widget.attrs.update({'class' : 'form-control'})
        self.fields['saldo'].widget.attrs.update({'class' : 'form-control'})
        self.fields['id'].widget.attrs.update({'class' : 'form-control'})

    grupos = [(choice.id, choice.nombre) for choice in Grupos.objects.all()]
    grupos = [('', 'Seleccione')] + grupos
    grupo_id = forms.ChoiceField(required=True, choices=grupos)
    estatus = forms.ChoiceField(required=True, choices=Cuentas.array_select_estatus)
    monto = forms.DecimalField(required=True, max_digits=15, decimal_places=2)
    saldo = forms.DecimalField(required=True, max_digits=15, decimal_places=2)
    id = forms.CharField(required=True, max_length=5)

    def clean_id(self):
        id = self.cleaned_data.get("id").upper()
        if Cuentas.objects.filter(id=id).exists():
            raise forms.ValidationError("ya existe")
        return id

class agregar_pago(forms.Form):
    def __init__(self, *args, **kwargs):
        super(agregar_pago, self).__init__(*args, **kwargs)
        self.fields['monto'].widget.attrs.update({'class' : 'form-control'})
        self.fields['fecha'].widget.attrs.update({'class' : 'form-control', 'placeholder': 'YYYY-MM-DD H:i:s'})
    
    monto = forms.DecimalField(required=True, max_digits=15, decimal_places=2)
    fecha = forms.DateTimeField(required=True)
    

