-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-11-2020 a las 04:57:33
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `podemos_eval`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can view log entry', 1, 'view_logentry'),
(5, 'Can add permission', 2, 'add_permission'),
(6, 'Can change permission', 2, 'change_permission'),
(7, 'Can delete permission', 2, 'delete_permission'),
(8, 'Can view permission', 2, 'view_permission'),
(9, 'Can add group', 3, 'add_group'),
(10, 'Can change group', 3, 'change_group'),
(11, 'Can delete group', 3, 'delete_group'),
(12, 'Can view group', 3, 'view_group'),
(13, 'Can add user', 4, 'add_user'),
(14, 'Can change user', 4, 'change_user'),
(15, 'Can delete user', 4, 'delete_user'),
(16, 'Can view user', 4, 'view_user'),
(17, 'Can add content type', 5, 'add_contenttype'),
(18, 'Can change content type', 5, 'change_contenttype'),
(19, 'Can delete content type', 5, 'delete_contenttype'),
(20, 'Can view content type', 5, 'view_contenttype'),
(21, 'Can add session', 6, 'add_session'),
(22, 'Can change session', 6, 'change_session'),
(23, 'Can delete session', 6, 'delete_session'),
(24, 'Can view session', 6, 'view_session');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$180000$QWIXMNGvjhSP$0PBUStrQZAB3oY38+6JNIVFSpi8epLOuZ8Ucpxy3YvE=', '2020-11-01 21:51:36.531054', 1, 'adelatorre', '', '', 'madgmadg@gmail.com', 1, 1, '2020-10-31 13:45:13.901422'),
(2, '', NULL, 0, 'prueba', '', '', 'prueba@prueba.com', 0, 1, '2020-10-31 14:28:25.148649');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calendariopagos`
--

CREATE TABLE `calendariopagos` (
  `id` int(11) NOT NULL,
  `cuenta_id` varchar(5) NOT NULL,
  `num_pago` int(11) NOT NULL,
  `monto` decimal(15,2) NOT NULL,
  `fecha_pago` date NOT NULL,
  `estatus` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `calendariopagos`
--

INSERT INTO `calendariopagos` (`id`, `cuenta_id`, `num_pago`, `monto`, `fecha_pago`, `estatus`) VALUES
(1, '23001', 1, '15000.00', '2018-11-30', 'PAGADO'),
(2, '23001', 2, '15000.00', '2018-12-07', 'PAGADO'),
(3, '23001', 3, '15000.00', '2018-12-14', 'PAGADO'),
(4, '23001', 4, '15000.00', '2018-12-21', 'PAGADO'),
(5, '12345', 1, '18750.00', '2018-12-20', 'PARCIAL'),
(6, '12345', 2, '18750.00', '2018-12-27', 'PENDIENTE'),
(7, '12345', 3, '18750.00', '2019-01-03', 'PENDIENTE'),
(8, '12345', 4, '18750.00', '2019-01-10', 'PENDIENTE'),
(9, '10001', 1, '37500.00', '2018-12-07', 'PAGADO'),
(10, '10001', 2, '37500.00', '2018-12-14', 'PAGADO'),
(11, '10001', 3, '37500.00', '2018-12-21', 'PARCIAL'),
(12, '10001', 4, '37500.00', '2018-12-28', 'PENDIENTE'),
(13, '89752', 1, '20000.00', '2019-01-07', 'ATRASADO'),
(14, '89752', 2, '20000.00', '2019-01-14', 'PENDIENTE'),
(15, '89752', 3, '20000.00', '2019-01-21', 'PENDIENTE'),
(16, '89752', 4, '20000.00', '2019-01-28', 'PENDIENTE'),
(17, '10004', 1, '30000.00', '2020-11-08', 'PENDIENTE'),
(18, '10005', 1, '30000.00', '2020-11-08', 'PENDIENTE'),
(19, '10006', 1, '30000.00', '2020-11-08', 'PENDIENTE'),
(20, '10007', 1, '30000.00', '2020-11-08', 'PENDIENTE'),
(21, '10007', 2, '30000.00', '2020-11-15', 'PENDIENTE'),
(22, '10007', 3, '30000.00', '2020-11-22', 'PENDIENTE'),
(23, '10007', 4, '30000.00', '2020-11-29', 'PENDIENTE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` varchar(7) NOT NULL,
  `nombre` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `nombre`) VALUES
('ABCDE03', 'IRMA MARQUEZ RUBIOs'),
('ABCDE04', 'MARCO ANDRES'),
('ABCDE05', 'ELIEZE EROSA'),
('ABCDE06', 'ELIEZE EROSA'),
('ASDFG08', 'SANDRA SANCHEZ GONZALEZ'),
('HJKLL09', 'ANGELA GOMEZ MONROY'),
('MNOPQ01', 'GERTRUDIS LOPEZ MARTINEZ'),
('NMZXC11', 'DANIELA HERNANDEZ GUERRERO'),
('OPQRS04', 'ALEIDA SANCHEZ AMOR'),
('QRSTU02', 'FERNANDA JUAREZ LOPEZ'),
('QWERT06', 'ALBA PEREZ TORRES'),
('TYUIQ05', 'LORENA GARCIA ROCHA'),
('YUIOP07', 'ELISEO CHAVEZ OLVERA'),
('ZXCVB10', 'KARLA ENRIQUEZ NAVARRETE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas`
--

CREATE TABLE `cuentas` (
  `id` varchar(5) NOT NULL,
  `grupo_id` varchar(5) NOT NULL,
  `estatus` varchar(15) NOT NULL,
  `monto` decimal(15,2) NOT NULL,
  `saldo` decimal(15,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cuentas`
--

INSERT INTO `cuentas` (`id`, `grupo_id`, `estatus`, `monto`, `saldo`) VALUES
('10001', 'XYZW1', 'CERRADA', '150000.00', '74500.00'),
('10002', 'FASFD', 'CERRADA', '120000.00', '1321321.00'),
('10003', 'FASFD', 'CERRADA', '120000.00', '1321321.00'),
('10004', 'FASFD', 'CERRADA', '120000.00', '1321321.00'),
('10005', 'FASFD', 'CERRADA', '120000.00', '1321321.00'),
('10006', 'FASFD', 'CERRADA', '120000.00', '1321321.00'),
('10007', 'FASFD', 'CERRADA', '120000.00', '1321321.00'),
('12345', 'ABCD2', 'DESEMBOLSADA', '75000.00', '64500.00'),
('23001', 'XYZW1', 'CERRADA', '60000.00', '0.00'),
('89752', 'GHIJK', 'DESEMBOLSADA', '80000.00', '80000.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(4, 'auth', 'user'),
(5, 'contenttypes', 'contenttype'),
(6, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2020-10-29 00:54:33.333131'),
(2, 'auth', '0001_initial', '2020-10-29 00:54:34.919607'),
(3, 'admin', '0001_initial', '2020-10-29 00:54:40.548505'),
(4, 'admin', '0002_logentry_remove_auto_add', '2020-10-29 00:54:42.039080'),
(5, 'admin', '0003_logentry_add_action_flag_choices', '2020-10-29 00:54:42.109191'),
(6, 'contenttypes', '0002_remove_content_type_name', '2020-10-29 00:54:43.062758'),
(7, 'auth', '0002_alter_permission_name_max_length', '2020-10-29 00:54:44.079498'),
(8, 'auth', '0003_alter_user_email_max_length', '2020-10-29 00:54:44.899450'),
(9, 'auth', '0004_alter_user_username_opts', '2020-10-29 00:54:44.961092'),
(10, 'auth', '0005_alter_user_last_login_null', '2020-10-29 00:54:45.459576'),
(11, 'auth', '0006_require_contenttypes_0002', '2020-10-29 00:54:45.490224'),
(12, 'auth', '0007_alter_validators_add_error_messages', '2020-10-29 00:54:45.553109'),
(13, 'auth', '0008_alter_user_username_max_length', '2020-10-29 00:54:46.448910'),
(14, 'auth', '0009_alter_user_last_name_max_length', '2020-10-29 00:54:47.145999'),
(15, 'auth', '0010_alter_group_name_max_length', '2020-10-29 00:54:48.518449'),
(16, 'auth', '0011_update_proxy_permissions', '2020-10-29 00:54:48.638275'),
(17, 'sessions', '0001_initial', '2020-10-29 00:54:49.378226');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('b5q0dg1yvaai24d2v52wn21q6vd30lw4', 'YjAxMjA5ZjFhMDBjOGRlOGQ5OTBhNzRiZTYyYTA5MTg2YTg1NzU3YTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5NzgzNzdlN2U5NTg3Y2M2ZDI5NmU2MjA4Mzg4OTBiN2NkZTcwZmY0In0=', '2020-11-15 21:51:36.801399');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupos`
--

CREATE TABLE `grupos` (
  `id` varchar(5) NOT NULL,
  `nombre` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `grupos`
--

INSERT INTO `grupos` (`id`, `nombre`) VALUES
('ABCD2', 'ANGELES DE CHARLIE'),
('FASFD', 'POWER RANGERS'),
('GHIJK', 'KITTIE'),
('XYZW1', 'POWERPUFF GIRLS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `miembros`
--

CREATE TABLE `miembros` (
  `id` int(11) NOT NULL,
  `grupo_id` varchar(5) NOT NULL,
  `cliente_id` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `miembros`
--

INSERT INTO `miembros` (`id`, `grupo_id`, `cliente_id`) VALUES
(1, 'ABCD2', 'ABCDE03'),
(2, 'ABCD2', 'ASDFG08'),
(3, 'ABCD2', 'QWERT06'),
(4, 'ABCD2', 'TYUIQ05'),
(6, 'GHIJK', 'HJKLL09'),
(7, 'GHIJK', 'NMZXC11'),
(8, 'GHIJK', 'ZXCVB10'),
(9, 'XYZW1', 'ABCDE03'),
(10, 'XYZW1', 'MNOPQ01'),
(11, 'XYZW1', 'OPQRS04'),
(12, 'XYZW1', 'QRSTU02'),
(14, 'FASFD', 'ABCDE04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transacciones`
--

CREATE TABLE `transacciones` (
  `id` int(11) NOT NULL,
  `cuenta_id` varchar(5) NOT NULL,
  `fecha` datetime NOT NULL,
  `monto` decimal(15,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `transacciones`
--

INSERT INTO `transacciones` (`id`, `cuenta_id`, `fecha`, `monto`) VALUES
(1, '23001', '2018-11-30 10:36:00', '15000.00'),
(2, '23001', '2018-12-07 12:50:00', '15000.00'),
(3, '23001', '2018-12-14 13:45:00', '15000.00'),
(4, '23001', '2018-12-21 11:35:00', '15000.00'),
(5, '12345', '2018-12-20 14:50:00', '5000.00'),
(6, '12345', '2018-12-21 15:25:00', '5500.00'),
(7, '10001', '2018-12-07 11:34:00', '37500.00'),
(8, '10001', '2018-12-07 10:04:00', '37500.00'),
(9, '10001', '2018-12-07 18:50:00', '-30000.00'),
(10, '10001', '2018-12-07 18:51:00', '-7500.00'),
(11, '10001', '2018-12-14 09:59:00', '37500.00'),
(12, '10001', '2018-12-21 11:05:00', '500.00'),
(13, '10001', '2020-11-01 18:58:00', '10000.00'),
(14, '10001', '2020-11-01 18:58:00', '55500.00'),
(16, '10001', '2020-11-01 18:58:00', '9000.00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indices de la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indices de la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indices de la tabla `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indices de la tabla `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indices de la tabla `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indices de la tabla `calendariopagos`
--
ALTER TABLE `calendariopagos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_CalendarioPagos_1_idx` (`cuenta_id`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cuentas`
--
ALTER TABLE `cuentas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_grupo_idx` (`grupo_id`);

--
-- Indices de la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indices de la tabla `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indices de la tabla `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indices de la tabla `grupos`
--
ALTER TABLE `grupos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `miembros`
--
ALTER TABLE `miembros`
  ADD PRIMARY KEY (`grupo_id`,`cliente_id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_cliente_idx` (`cliente_id`);

--
-- Indices de la tabla `transacciones`
--
ALTER TABLE `transacciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Transacciones_1_idx` (`cuenta_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de la tabla `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `calendariopagos`
--
ALTER TABLE `calendariopagos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `miembros`
--
ALTER TABLE `miembros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `transacciones`
--
ALTER TABLE `transacciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Filtros para la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Filtros para la tabla `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Filtros para la tabla `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Filtros para la tabla `calendariopagos`
--
ALTER TABLE `calendariopagos`
  ADD CONSTRAINT `fk_CalendarioPagos_1` FOREIGN KEY (`cuenta_id`) REFERENCES `cuentas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cuentas`
--
ALTER TABLE `cuentas`
  ADD CONSTRAINT `fk_Cuentas_1` FOREIGN KEY (`grupo_id`) REFERENCES `grupos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Filtros para la tabla `miembros`
--
ALTER TABLE `miembros`
  ADD CONSTRAINT `fk_cliente` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_grupo` FOREIGN KEY (`grupo_id`) REFERENCES `grupos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `transacciones`
--
ALTER TABLE `transacciones`
  ADD CONSTRAINT `fk_Transacciones_1` FOREIGN KEY (`cuenta_id`) REFERENCES `cuentas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
