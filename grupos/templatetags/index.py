from django import template

register = template.Library()

def index(lista, id):
    return lista[id-1]

register.filter('index', index)