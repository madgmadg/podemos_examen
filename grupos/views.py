from django.shortcuts import render, redirect
from django.contrib import messages
from django.http import JsonResponse
from grupos.models import Grupos, Miembros
from clientes.models import Clientes
from formsClass.clientesform import grupos_form, grupos_form_update, agregar_cliente

# Create your views here.

def index(request):
    return render(request, 'grupos/index.html')

def listado(request):
    model = list(Grupos.objects.values())
    return JsonResponse({'data': model}, safe=False)

def create(request):
    form = grupos_form()
    if request.method == "POST":
        form = grupos_form(data=request.POST)
        if form.is_valid():
            model = Grupos(
                id=request.POST['id'].upper(),
                nombre=request.POST['nombre'],
            )
            model.save()
            messages.success(request, 'Creado con éxito!!')
            return redirect('/grupos/index')
    return render(request, 'grupos/create.html', {
        'form': form
    })

def update(request, id):
    model = Grupos.objects.filter(id=id)
    if model.count() == 0:
        return redirect('error/404')
    model = model.get()
    form = grupos_form_update(instance=model)
    if request.method == "POST":
        form = grupos_form_update(data=request.POST)
        if form.is_valid():
            model.nombre = request.POST['nombre']
            model.save()
            messages.success(request, 'Actualizado con éxito!!')
            return redirect('/grupos/view/' + str(model.id))
    return render(request, 'grupos/update.html',{
        'form': form,
        'model': model
    })

def view(request, id):
    model = Grupos.objects.filter(id=id)
    if model.count() == 0:
        return redirect('error/404')
    model = model.get()
    miembros = model.miembros_set
    miembros_list_id = []
    miembros_list = []
    if miembros.count() > 0:
        i = 0
        for value in miembros.all():
            cliente = Clientes.objects.get(id=value.cliente_id)
            miembros_list.insert(i, cliente.nombre)
            miembros_list_id.insert(i, cliente.id)
            i += 1
    cuentas = model.cuentas_set.all()
    deuda = model.suma_deuda()
    return render(request, 'grupos/view.html',{
        'model': model,
        'miembros_list': miembros_list,
        'miembros_list_id': miembros_list_id,
        'cuentas': cuentas
    })

def elimina_cliente(request, id, id_cliente):
    model = Miembros.objects.filter(grupo_id=id,cliente_id=id_cliente)
    if model.count() == 0:
        return redirect('error/404')
    model = model.get()
    model.delete()
    messages.success(request, 'Eliminado con éxito!!')
    return redirect('/grupos/view/' + str(id))

def agregarcliente(request, id):
    model = Grupos.objects.filter(pk=id)
    if model.count() == 0:
        return redirect('error/404')
    model = model.get()
    form = agregar_cliente()
    if request.method == 'POST':
        form = agregar_cliente(data=request.POST)
        if form.is_valid():
            busca = Miembros.objects.filter(
                grupo_id=model.id,
                cliente_id=request.POST['cliente_id']
            )
            if busca.count() > 0:
                messages.error(request, 'El cliente ya esta asignado a este grupo!!')
                return redirect('/grupos/agregarcliente/' + str(model.id))
            miembros = Miembros(
                grupo_id=model.id,
                cliente_id=request.POST['cliente_id']
            )
            miembros.save()
            messages.success(request, 'El cliente se asigno correctamente!!')
            return redirect('/grupos/view/' + str(model.id))
    return render(request, 'grupos/agregarcliente.html', {
        'model': model,
        'form': form
    })
    import pdb
    pdb.set_trace()