from django.db import models
from clientes.models import Clientes
# Create your models here.

class Grupos(models.Model):
    id = models.CharField(primary_key=True, max_length=5)
    nombre = models.CharField(null=True, max_length=20)

    class Meta:
        managed = False
        db_table = 'grupos'

class Miembros(models.Model):
    id = models.IntegerField(primary_key=True)
    grupo = models.ForeignKey(Grupos, models.DO_NOTHING)
    #cliente_id = models.CharField(null=True, max_length=7)
    cliente = models.ForeignKey(Clientes, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'miembros'