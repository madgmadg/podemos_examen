from django.db import models
from grupos.models import Grupos
from django.db.models import Sum

# Create your models here.

class Cuentas(models.Model):

    ESTATUS_CERRADA = 'CERRADA'
    ESTATUS_DESEMBOLSADA = 'DESEMBOLSADA'

    array_select_estatus = [
        ['', 'Seleccione'],
        [ESTATUS_CERRADA, ESTATUS_CERRADA],
        [ESTATUS_DESEMBOLSADA, ESTATUS_DESEMBOLSADA],
    ]

    id = models.CharField(primary_key=True, max_length=5, unique=True)
    grupo = models.ForeignKey(Grupos, models.DO_NOTHING)
    estatus = models.CharField(max_length=15)
    monto = models.DecimalField(max_digits=15, decimal_places=2)
    saldo = models.DecimalField(max_digits=15, decimal_places=2)
    
    class Meta:
        managed = False
        db_table = 'cuentas'