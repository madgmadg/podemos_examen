from django.shortcuts import render, redirect
from cuentas.models import Cuentas
from calendariopagos.models import Calendariopagos
from django.http import JsonResponse
from formsClass.clientesform import agregar_cuentas
from django.contrib import messages
import datetime

# Create your views here.

def index(request):
    return render(request, 'cuentas/index.html')

def listado(request):
    model = list(Cuentas.objects.values())
    return JsonResponse({'data': model}, safe=False)
    
def create(request):
    form = agregar_cuentas()
    if request.method == "POST":
        form = agregar_cuentas(data=request.POST)
        if form.is_valid():
            model = Cuentas(
                id=request.POST['id'],
                grupo_id=request.POST['grupo_id'],
                estatus=request.POST['estatus'],
                saldo=request.POST['saldo'],
                monto=request.POST['monto']
            )
            model.save()
            divide_monto = float(request.POST['monto']) / Calendariopagos.NUMERO_PAGOS
            fecha = datetime.datetime.now()
            for value in range(0, Calendariopagos.NUMERO_PAGOS):
                dias = 7 * (value + 1)
                fecha_suma = fecha + datetime.timedelta(days=dias)
                calendariopagos = Calendariopagos(
                    cuenta_id=model.id,
                    num_pago=value+1,
                    monto=divide_monto,
                    fecha_pago=fecha_suma.strftime("%Y-%m-%d"),
                    estatus=Calendariopagos.ESTATUS_PENDIENTE
                )
                calendariopagos.save()
            messages.success(request, 'Cuenta creada con éxito!!')
            return redirect('/cuentas/view/' + str(model.id))
    return render(request, 'cuentas/create.html', {
        'form': form
    })

def view(request, id):
    model = Cuentas.objects.filter(id=id)
    if model.count == 0:
        return redirect('error/404')
    model = model.get()
    calendariopagos = model.calendariopagos_set.all()
    transacciones = model.transacciones_set.all()
    return render(request, 'cuentas/view.html', {
        'model': model,
        'calendariopagos': calendariopagos,
        'transacciones': transacciones,
    })